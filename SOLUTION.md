# Read & parse all the data provided
Reading and parsing the data was fairly straight forward. I created a function, read_file, which
calls a Utils method to validate the file (validate it's path, and that it is a file and not a dir).
Then we needed to iterate over the data and using json.loads(d) to convert it to an easily
parseable dictionary object.

The parsing function iterates over the data we read in and separates them by their disposition. 
For the unknown case, I wanted to take advantage of this loop to keep count of the duplicate
SHA and create a list that I could later use len() on to get a count of how many times that specific
file showed up with an unknown disposition. This would be used later on, when I want to update or
insert into the database.

For the parsing element, there was an observed case where the disposition is not 1, 2, or 3 (one of
the expected dp numbers) so I decided that if it was an unknown disposition, then the entry itself
should be labeled as unknown. However, we would want to try and traceback why the disposition number
was being recorded incorrectly and therefore keep track of these abnormalities.

# For each entry marked with an UNKNOWN disposition, perform a database lookup to determine:
�	Whether the file has been seen before
o	if so increment the database count (`cnt`) by 1
�	Whether the file is new
o	if so insert a new entry with `count == 1` and disposition (`dp`) == UNKNOWN

For this solution, we first needed to query the database and find out what SHA's already exist and
to create a list with these entries. With this list, we can loop over our unknown entries that we
parsed in earlier and for each one of those check if it is in our database. If we find that it is,
then we just perform a simple update where we increment our count by one. If we do not find it is
in the database, then we will insert our entry into the database.

For our insert query, this is where can take advantage of Python's built-in len() to get the size
of the array and use that as our count.

After we perform all of our updates and inserts, we then make a call to our db.close_conn() function,
which commits the changes we made and gracefully closes down our db connection.

# We know the cloud server is malfunctioning, what types of patterns or anomalies did you observe?
For this part of the problem, I decided to focus on the hint, which was file paths. The approach I
decided to take was to analyze all of the clean entries and to build some type of rules based on the
paths of the clean entries. 

To do this, I implemented a type of DFA/Grammar hybrid that kept track of states via their index within a 
list. For each iteration, I would have to keep track of the previous element and previous state, because
I would be using this to determine where the current element belongs. A simple example of what the data
structure looks like:

	<home> :: <user> | <lib> | <bin>
	<user> :: <nick> | <root>
	<lib>  :: <php> | <python>
	<bin> :: <bash> | <sh>
	<python> :: pip | python3.exe

In the Analysis class I built, the build_clean_paths essentially defines the rules like the above example.
We can then take these rules and using the verify_malicious_path() function, it will iterate over each element
separated by the forward slash ("/") and with a state variable track whether or not that element fits within
the rules defined above.

Once we encounter a condition where the element is not following the rules, we can break out of the loop and continue
on with the next entry. For providing some context, I included 2 variables that keep track of how many good file
paths I found and bad (unknown) file paths I found. For automated analysis, we could use this count to really
determine via a log file from our cloud server, if the incidents are isolated or if they are widespread and we
need to start performing some maintenance. 

### If possible, what types of problem hypothesis can you form from your observations?

I observed some anomalies in the malicious entries:

* The first I observed was that there were entries in the log that did not have a good disposition (was not 1, 2, or 3)
* I also observed that some of the malicious entries had a SHA256 that was not 64 characters long which would
mean that it is an invalid SHA256 number.
* For the analysis of the file paths, with the parser that I built, I found that 32845 of the file paths 
could not be verified with our predetermined rules that were built from the clean entries.
  I was able to verify 205 of those entries and determine that their path was an observed path from the clean entries.
  
# Final thoughts
I think given the current state of this application, I would certainly release it as version 1 if there were
very tight deadlines for the application. And given that it could mostly be run as an independent service
separated from the primary infrastructure, it could be sandboxed so that any fatal errors do not harm or interfere
with other applications.

Things I would like to have added:
 * Unit tests
 * Caching function - to store the grammar we build and tie it to the particular log file
 * Add more analysis to find SHA256 file names that do not fit the description of a SHA256 file
 * Handle the cases where disposition is completely incorrect