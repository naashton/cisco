import os

from cisco_utils import Utils

'''
Writing down thoughts here. Unorganized and semi-unfiltered.

What should we do when we instantiate this object?
Thoughts:
  * Pass in the list of clean entries and the list of malicious entries
  * Use the list of clean entries to build our grammar with the file paths
  * This grammar will be stored as a 2d array
  * Then perform a reverse lookup with the malicious list to see if any of the
    paths matches an acceptable path
'''
'''
NEW DESIGN:
[{elem : [next_path, next_path2]},
{next_path : [next_next_path], next_path2 : [next_next_path2, next_nextest_path]},
{next_next_path : [final_elem]},
{final_elem : []}]

This design fulfills the needs I was going for in trying to build a set of rules while
also keeping track of states in the form of indexes, which ends up creating a pseudo 
DFA/Grammar type algorithm.
'''
class Analysis:
    '''
    Use this to build our grammar rules and perform analysis of the elements in the entries to try
    and determine what makes the entry malicious
    '''
    
    def __init__(self, clean_entries, malicious_entries):
        '''
        We need a few variables in this class.
          * clean_paths is our rules list which also acts as a DFA because we want to be able to 
          determine if the current element fits within the constraints of our rules set for
          the previous element
          * clean_entries is just a list of the clean entries we parsed from the log file
          * malicious_entries is the list of malicious entries parsed from the log file
          * verified_count is useful if we want to deliver a report from our analysis and give a count
          * unverified_count has the same use as above, but for the unverified paths
        '''
        self.clean_paths = []
        self.clean_entries = clean_entries
        self.malicious_entries = malicious_entries
        self.verified_count = 0
        self.unverified_count = 0
    
    def build_clean_paths(self):
        '''
        build_clean_paths
        Use the clean_entries to build a table made up of all *known* permutations of a file path
        '''
        Utils.debug_print("Building clean paths...")
        for entry in self.clean_entries:
            # Grab the 'ph' value and split the file path by the path seperator
            path = entry.get('ph')
            path_elems = path.split("/")[1::]
            prev_state = None
            prev_elem = None
            # Grab the index and element
            for i, elem in enumerate(path_elems):
                if len(self.clean_paths)-1 >= i:
                    if elem not in self.clean_paths[i]:
                        self.clean_paths[i][elem] = []
                else:
                    self.clean_paths.insert(i, {elem : []})
                    
                # The elem becomes a value of the prev value
                if prev_state is not None and prev_elem is not None:
                    self.clean_paths[prev_state][prev_elem].append(elem)
                prev_state = i
                prev_elem = elem
                
        Utils.debug_print("Finished building clean paths.")
        return
    
    def verify_malicious_path(self):
        '''
        verify_malicious_path
        This method will handle the algorithm needed to check to see if a malicious entries file path
        can be verified by our clean_paths table.
        
        It is important to note, this does not definitively determine that a file path is invalid. Simply
        it is a file path that we have not verified via our clean_entries.
        '''
        Utils.debug_print("Verifying malicious paths...")
        for entry in self.malicious_entries:
            path = entry.get('ph')
            path_elems = path.split("/")[1::]
            prev_state = None
            prev_elem = None
            for i, elem in enumerate(path_elems):
                # First, check if the element is not in the index of the clean_paths list and break
                # out of the loop since we don't recognize that directory at that index
                if elem not in self.clean_paths[i]:
#                     Utils.debug_print("Unrecognized element")
                    self.unverified_count += 1
                    break
                if prev_state is not None and prev_elem is not None:
                    # Check all elements not at index 0 to see if they are an accepted value
                    # of the previous element at the previous index and break if it IS NOT
                    if elem not in self.clean_paths[prev_state][prev_elem]:
#                         Utils.debug_print("Path does not fit the predefined rules")
                        self.unverified_count += 1
                        break
                
                if i == len(path_elems)-1:
                    self.verified_count += 1
#                     Utils.debug_print("We have verified that the path exists in this system.")
                prev_state = i
                prev_elem = elem
            
        return
    
        