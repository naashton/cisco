# Python imports
import json
import os
import sys
import time

# Custom imports
from cisco_utils import Utils
from cisco_db_conn import Database
from cisco_analysis import Analysis


class LogDiagnostics:
    '''
    This class is for purposes performing diagnostics against log files.
     * Validate 
     * Read in the log file
    '''
    
    def __init__(self, file):
        '''
        Constructor:
        Given a file, validate that it is a json file and read in the contents
        '''
        self.data = None
        self.malicious_entries = []
        self.clean_entries = []
        self.unknown_entries = {}
        
        # Initialize our database
        self.db = Database()
        
        if Utils.validate_file(file):
            self.read_file(file)
        else:
            Utils.debug_print("Error: Invalid Log File: " + file,
                              "\nPlease check that the file exists and is not corrupted.")
            sys.exit()
        
            
    def read_file(self, file):
        '''
        read_file:
         * Verify the path and existence of the file
         * Open the file using 'with' context and readlines
             @attention: This reads each line into a list as a string. We need to convert it
             to a dictionary to access the data more easily.
         * Using list comprehension, we can iterate over each entry in self.data and use json.loads(entry)
           to encode that as a dictionary object.
        '''
        if Utils.validate_file(file):
            try:
                with open(file) as logf:
                    self.data = logf.readlines()
            except:
                Utils.debug_print("Encountered an error")
                
        if self.data is not None:
            # This nicely loads each entry as a dictionary object (from string) 
            self.data = [json.loads(d) for d in self.data]
        return
    
    def parse_logs(self):
        '''
        parse_logs:
         * Iterate over the list
         * Grab each log entry
         * Separate each entry by it's dp(disposition) and store them in a list for work later in the program
        '''
        if self.data is not None:
            # Do work
            for entry in self.data:
                if entry['dp'] == Utils.DP_MALICIOUS:
                    self.malicious_entries.append(entry)
                elif entry['dp'] == Utils.DP_CLEAN:
                    self.clean_entries.append(entry)
                elif entry['dp'] == Utils.DP_UNKNOWN:
                    '''
                    @attention: This elif and the else statement do the same thing, but I am making an assumption
                    based on the information that for disposition there should only be 3 values (1, 2, or 3) and 
                    given that, any entry with a disposition that is NOT one of those, by default, has unknown 
                    characteristics.
                    
                    The reasons for separating them is for a future case where we want to start handling them
                    differently.
                    '''
                    # For unk entries, use a dict. We can use len() to get the count of occurences and use that for our insert/update
                    if entry['sha'] in self.unknown_entries:
                        self.unknown_entries[entry['sha']].append(entry)
                    else:
                        self.unknown_entries[entry['sha']] = [entry]
                else:
                    # If we encounter an entry were the dp is not 1, 2, or 3 we should include it with the
                    # unknown entries
                    if entry['sha'] in self.unknown_entries:
                        self.unknown_entries[entry['sha']].append(entry)
                    else:
                        self.unknown_entries[entry['sha']] = [entry]
                    pass
        else:
            # Write a less sarcastic debug print
            Utils.debug_print("Congrats. You really broke something.")
        return
    
    def handle_unknown_entries(self):
        '''
        handle_unknown_entries
        We are going to use this method to either update the count of the file, by it's sha256 encrypted key,
        or create a new entry if it does not exist.
        '''
        unk_db_entries =  self.db.select_query()
        
        # Iterate over the unknown_entries dict. Remember that 'entries' here will be a list where the len(entries) is the count
        start = time.time()
        for unk_sha, entries in self.unknown_entries.items():
            if unk_sha in unk_db_entries:
                self.db.update_query([(unk_sha)])
            else:
                self.db.insert_query([(unk_sha, len(entries), Utils.DP_UNKNOWN)])
        end = time.time()
        Utils.debug_print("Run time of our insert/update for loop: ", end-start)
        self.db.close_conn()
        return
    
    
    def analyze_malicious(self):
        '''
        Ok, what are some of the things we can look at here?
        
        Hint: From the README, check file paths.
          - We can use grammars to iterate over the "clean" list of entries and compile a list of
            ALL acceptable paths by separating each directory by the "/". So...
              * /home -> /user1 | /user2
              * /user1 -> /projects | /music | /documents
              * /user2 -> /pictures | /downloads
              * /projects -> /python_proj | /java_proj | some_file.txt
              * /music -> /rock | /rap | greatest_hit.mp3
            Much like grammars, we start at the begginning and start building a logical path based on
            acceptable next steps. Starting at /home we can go to either /user1 or /user2. And progress
            down the line until we have a path.
            
            How do we implement this?
        
        Check the sha256 and verify that it is the correct size. The sha256 hashing algorithm should consistently
        be the same length. (64 chars long?)
        
        Check file extensions. Create a list of acceptable file extensions and check those against that.
        
        Check timestamp. What is the date format?
        '''
        if self.clean_entries is None and self.malicious_entries is None:
            self.parse_logs()
        analyze = Analysis(self.clean_entries, self.malicious_entries)
        
        analyze.build_clean_paths()
        analyze.verify_malicious_path()
        
        print("Our analysis was able to verify ", analyze.verified_count, " paths based on our clean entries")
        print("Our analysis determined that ", analyze.unverified_count, " file paths were not accepted based on our rules")
        
        return
    
if __name__ == '__main__':
    file = os.getcwd() + os.path.sep + "data" + os.path.sep + "log.json"
    diag = LogDiagnostics(file)
    
    diag.parse_logs()

    diag.handle_unknown_entries()

    diag.analyze_malicious()