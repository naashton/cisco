import sqlite3

from cisco_utils import Utils

class Database:
    
    def __init__(self, db_file=None):
        '''
        If no value is passed, use the constant, LOCAL_DB_FILE from our Utils class.
        @param string db_file is the name of our .sqlite3 database file
        '''
        self.conn = None
        self.cursor = None
        if db_file is None:
            self.db_file = Utils.build_path(Utils.LOCAL_DB_FILE)
        else:
            self.db_file = Utils.build_path(db_file)
        
        # Initialize the database
        self.init_db()
        
    def init_db(self):
        '''
        init_db
        Initialize the database connection. 
        Catch the error if we are unable to connect and display a message to the user.
        '''
        try:
            self.conn = sqlite3.connect(self.db_file)
            self.cursor = self.conn.cursor()
            Utils.debug_print("Connected to DB.\n", self.conn)
        except sqlite.Error as e:
            Utils.debug_print("Encountered an error when trying to initialize the database:\n",
                              e)
        return
    
    def select_query(self):
        '''
        select_query
        We only need to query the table to get a list of sha256 keys in the table so that we can
        compare that list against sha256 keys we find in our log file.
        '''
        data = None
        result = None
        sql = "SELECT * FROM seen_malware WHERE dp=?"
        try:
            data = self.cursor.execute(sql, str(Utils.DP_UNKNOWN))
        except sqlite3.OperationalError as e:
            Utils.debug_print("Encountered an error when trying to execute the query: [", sql, "]\n"
                              "Error: ", e)
        
        # The query returns a tuple, so we will place them in a list to make the lookup easier.
        if data is not None:
            result = []
            for d in data:
                result.append(d[0])
        
        return result
    
    def insert_query(self, unk_entries):
        '''
        insert_query
        When we find an entry with "UNKNOWN" disposition, we want to insert that file into the database.
        @param unk_arr is just an array of our attributes we need to insert a new item into the table
            @attention: This is an array of tuples where the tuple has 3 values (sha256, count, dp)
        '''
        sql = "INSERT INTO seen_malware VALUES (?, ?, ?)"
        try:
            self.cursor.executemany(sql, unk_entries)
        except sqlite3.Error as e:
            Utils.debug_print("Encountered an error", e)
        return
    
    def update_query(self, sha):
        '''
        update_query
        When we find an entry with "UNKOWN" disposition and that entry already exists in the table, we
        need to increment the count.
        '''
        sql = "UPDATE seen_malware SET cnt=cnt+1 WHERE sha=?"
        try:
            self.cursor.execute(sql, (sha))
        except sqlite3.Error as e:
            Utils.debug_print("Encountered an error: ", e)
        return
    
    def close_conn(self):
        '''
        close_conn
        Commit changes we've made and close the connection explicitly.
        '''
        self.conn.commit()
        self.conn.close()
        return
    
if __name__ == '__main__':
    db = Database()
    c = db.conn.cursor()
#     conn = db.init_db()
#     cursor = db.conn.cursor()
#     c =  db.conn.cursor()
    data = c.execute("SELECT * FROM seen_malware")
    print(data)
    for i in data:
        print(i)

#     tabs = c.execute("SELECT * FROM sqlite_master") # WHERE type='table';")
#     for name in tabs:
#         print(name)
    
#     print(type(('val', 'example', '20')))
#     
#     print(db.insert_query([("TEST", "-1", "-1")]))
#     
#     print(db.insert_query([("TEST", "12345", "3")]))
#     
#     d = db.select_query()
#     print(d)
#     for i in d:
#         print(i)