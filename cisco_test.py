import os
from cisco_challenge import LogDiagnostics

def test():
    '''
    Creating a test case
    '''
    file = os.getcwd() + os.path.sep + "data" + os.path.sep + "log.json"
    diag = LogDiagnostics(file)
    
    diag.parse_logs()

    diag.handle_unknown_entries()

    diag.analyze_malicious()
    return

test()