import os
import sys


class Utils:
    '''
    Just a simple class with some utility functions. 
    This class will just contain class methods so we don't need a constructor.
    
    We can also include some constants in here that we might want to reference
    throughout our application.
    '''
    LOCAL_DB_FILE = "seen_malware.sqlite3"
    LOCAL_DATA_DIR = "data"
    
    DP_MALICIOUS = 1
    DP_CLEAN = 2
    DP_UNKNOWN = 3

    
    @classmethod
    def debug_print(cls, *args):
        '''
        debug_print
        This method gives us some more custom behavior that we can control via
        environment variables and take in additional arguments for more .
        
        @TODO: When we ship our code to production, we should log errors we might 
        encounter so that we can audit tracebacks and other issues with the program.
        
        @attention: The __debug__ global is toggled off (set to False) when the '-o' (optimized) option is passed
        when running python applications and thus will not print out anything when this method is called.
        
        Example:
        python -o cisco_utils.py 
        '''
        if __debug__:
            print(*args, file=sys.stderr)
        return
    
    @classmethod
    def validate_file(cls, file_path):
        '''
        validate_file
        This method should be used to verify a few things:
         * The file exists
         * The file is not a directory
        Return True if the file exists and it is a file
        '''
        if file_path:
            return(os.path.exists(file_path) and os.path.isfile(file_path))
        return False
    
    @classmethod
    def build_path(cls, file_name, file_path=None):
        '''
        We want to get the full system path and use python's os.path.sep to correctly handle
        differing operating system file paths (Linux using "/" and Windows using "\")
        '''
        if file_path is None:
            path = os.getcwd() + os.path.sep + cls.LOCAL_DATA_DIR + os.path.sep + file_name
        else:
            path = file_path + os.path.sep + file_name
        return path
        
    
if __name__ == '__main__':
    Utils.debug_print("This message\n", "is going to appear\n", "only in the stderr")